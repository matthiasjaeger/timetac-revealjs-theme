# TimeTac Reveal.js Theme #

Javascript framework for presenations, POI and conferences to be used by technical staff.

## Advice:
Contact Matthias Jäger in case of confusion: matthias.jaeger@timetac.com

## Setup from your terminal
* Make a new directory for your presentation
* * ``mkdir myPresentation``
* Move in that directory
* * ``cd myPresentation``
* Clone this repository to your local machine
* * `` git clone git@bitbucket.org:matthiasjaeger/timetac-revealjs-theme.git ``
* Edit the ``index.html`` or the ``demo.html`` with your content (I use Visual Studio Code)
* * `` code . ``


## Setup with downloaded copy
* Make a new directory for your presentation
* Download this repository to your local machine (Click the "..." icon in the upper right)
* Edit the ``index.html`` or the ``demo.html`` with your content

## Run the presentation!
* To *avoid* problems with Cross-Origin Resource Sharing run it from a *development server* don't use the File protocol.
* Use a plugin for VS Code (Live Server), Python (simple-server) or whatever you want

## TODOS
* [x] Write a README.md
* [ ] Include EOT fonts to make sure it runs on Linux properly?
* [ ] Test on different devices
* [ ] Add responsive logo svg in the header
* [ ] Make an inverted white-blue theme
* [ ] Move to timetac bitbucket

